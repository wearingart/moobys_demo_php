<?php
/*
 * This page is used to display the schedules from the Moobys database. It uses the Moobys\Schedule class to run the
 * main sql calls that add, insert, update, and delete data.
 *
 */

require 'Moobys/Schedule.php';
use Moobys\Schedule as mbySched;

ini_set('display_errors', 'On');


$page_id = "schedule";

if(isset($_POST["id"])) {
    $id = $_POST["id"];
}

require 'connect.php';

$mysqli = db_connect();

$stmt = '';

?>
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CS340 Project: Mooby's Food and Fun</title>

        <!-- Bootstrap core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="bower_components/datetimepicker/jquery.datetimepicker.css"/ >

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">

    </head>

    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Mooby's Database Admin</a>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class=<?php $class_value = ($page_id == "home") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="index.php">Overview</a></li>
                    <li class=<?php $class_value = ($page_id == "restaurant") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="restaurants.php">Restaurants</a></li>
                    <li class=<?php $class_value = ($page_id == "employees") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="employees.php">Employees</a></li>
                    <li class=<?php $class_value = ($page_id == "positions") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="positions.php">Positions</a></li>
                    <li class=<?php $class_value = ($page_id == "menu") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="menu.php">Menu Items</a></li>
                    <li class=<?php $class_value = ($page_id == "schedule") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="schedules.php">Work Schedules</a> </li>
                    <li class=<?php $class_value = ($page_id == "sales") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="sales.php">Sales</a> </li>
                </ul>
                <ul class="nav nav-sidebar">
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1>Mooby's Employee Schedules</h1>
<div id="test"></div>
            <div id="formAddSchedule" name="formAddSchedule">
                <form class="form-horizontal" method="POST" action="schedules.php">
                    <fieldset>
                        <legend>Enter Schedule</legend>


                        <div class="form-group">
                            <label class="col-md-2" >Name:</label>
                            <select class="col-md-3" name="eid" id="eid">
                                <option value="" hidden></option>
                                <?php
                                $positionSelect = '';
                                if (!($stmt = $mysqli->prepare('SELECT E.id, E.fname, E.lname, P.id, P.title, R.id, R.city FROM mby_employee E
                                                                INNER JOIN mby_position P ON E.position_id = P.id
                                                                INNER JOIN mby_restaurant R ON E.restaurant_id = R.id
                                                                ORDER BY E.lname ASC')))
                                {
                                    echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                }

                                if (!$stmt->execute()) {
                                    echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                if (!$stmt->bind_result($eid, $fname, $lname, $pid, $position, $rid, $city)) {
                                    echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                while ($stmt->fetch()) {
                                    echo "<option name=\"eid\" value=\"" . $eid . "\" data-pid=\"". $pid ."\" data-position=\"". $position ."\" data-rid=\"". $rid ."\" data-city=\"". $city ."\" data-fname=\"". $fname ."\" data-lname=\"". $lname ."\">" . $fname . " " . $lname . "</option>";
                                }
                                $stmt->close();

                                ?>
                            </select>
                            <input type="hidden" id="fname" name="fname" value="">
                            <input type="hidden" id="lname" name="lname" value="">
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" >Position:</label>
                            <input class="col-md-3" type="text" id="position" name="position" readonly="readonly" value="">
                            <input type="hidden" id="pid" name="pid" value="">

                        </div>

                        <div class="form-group">
                            <label class="col-md-2" >Restaurant Location:</label>
                            <input class="col-md-3" type="text" id="city" name="city" readonly="readonly" value="">
                            <input type="hidden" id="rid" name="rid" value="">
                        </div>

                        <div class="form-group">
                            <label class="col-md-2" >Date:</label>
                            <input id="datetimepicker-1" type="text" name="date">
                        </div>

                        <div class="form-group">
                            <label class="col-md-2" >Start Time:</label>
                            <input id="datetimepicker-2" type="text" name="starttime">
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" >End Time:</label>
                            <input id="datetimepicker-3" type="text" name="endtime">
                        </div>

                        <input type="hidden" name="mode" value="add">
                        <input name="submit" value="Schedule Employee" class="btn btn-primary" type="submit">
                    </fieldset>
                </form>

            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Date</th>
                        <th>Location</th>
                        <th>Employee ID</th>
                        <th>Name</th>
                        <th></th>
                        <th>Position</th>
                        <th>Start Time</th>
                        <th>End Time</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    //contains basic logic for delete, update, and setting mode
                    include "includes/logic.inc.php";
                    unset($params);
                    unset($data);

                    //used in insert and update functions. mode distinguishes which is used.
                    if (isset($_POST['date']) && !empty($_POST['date'])) {
                        $postDate = $_POST['date'];
                    }
                    if (isset($_POST['fname']) && !empty($_POST['fname'])) {
                        $postFirstName = $_POST['fname'];
                    }
                    if (isset($_POST['lname']) && !empty($_POST['lname'])) {
                        $postLastName = $_POST['lname'];
                    }
                    if (isset($_POST['city']) && !empty($_POST['city'])) {
                        $postCity = $_POST['city'];
                    }
                    if (isset($_POST['position']) && !empty($_POST['position'])) {
                        $postPosition = $_POST['position'];
                    }
                    if (isset($_POST['starttime']) && !empty($_POST['starttime'])) {
                        $postStart = $_POST['starttime'];
                    }
                    if (isset($_POST['endtime']) && !empty($_POST['endtime'])) {
                        $postEnd = $_POST['endtime'];
                    }
                    if (isset($_POST['eid']) && !empty($_POST['eid'])) {
                        $postEID = $_POST['eid'];
                    }


                    $getSql = 'SELECT S.date, R.city, S.employee_id, E.fname, E.lname, P.title, S.start_time, S.end_time, R.id, P.id, S.id
                                FROM mby_schedule S INNER JOIN mby_employee E ON S.employee_id = E.id
                                INNER JOIN mby_position P ON E.position_id = P.id
                                INNER JOIN mby_restaurant R ON E.restaurant_id = R.id
                                ORDER BY S.date DESC, R.city ASC, P.title ASC';
                    $getParams = array(['date', 'city','eid', 'fname','lname','position', 'starttime', 'endtime', 'rid', 'pid','id']);

                    if ($modeSet) {
                        if ($mode === "add") {
                            $sql = 'INSERT INTO mby_schedule(employee_id, date, start_time, end_time) VALUES (?,?,?,?)';
                            $data = ['eid'=>$postEID, 'date'=>$postDate, 'starttime'=>$postStart, 'endtime'=>$postEnd];
                            $params = 'isss';
                            $action = new mbySched($data, $params, $sql, $mysqli);
                            insertSched($action);

                            $action = new mbySched('', $getParams, $getSql, $mysqli);
                            getSched($action);
                            unset($action);
                        }
                        elseif ($mode === "delete") {
                            $sql = 'DELETE FROM mby_schedule WHERE id = ?';
                            $params = 'i';
                            $action = new mbySched($getID, $params, $sql, $mysqli);
                            deleteSched($action);

                            $action = new mbySched('', $getParams, $getSql, $mysqli);
                            getSched($action);
                            unset($action);
                        }
                        elseif ($mode === "edit") {
                            $action = new mbySched('', $getParams, $getSql, $mysqli);
                            getSched($action);
                            unset($action);
                        }
                        elseif ($mode === "update") {
                            unset($data);
                            $data = ['date'=>$postDate, 'starttime'=>$postStart, 'endtime'=>$postEnd, 'id'=>$postID];
                            $sql = 'UPDATE mby_schedule SET date = ?, start_time = ?, end_time = ? WHERE id = ?';
                            $params = 'sssi';
                            $action = new mbySched($data, $params, $sql, $mysqli);
                            updateSched($action);

                            $action = new mbySched('', $getParams, $getSql, $mysqli);
                            getSched($action);
                            unset($action);
                        }
                    }
                    else {

                        $action = new mbySched('', $getParams, $getSql, $mysqli);
                        getSched($action);
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function getSched($action)
                    {
                        $rData = $action->show();
                        unset($action);
                        echo $rData;
                    }

                    /**
                     * @param $action
                     */
                    function insertSched($action)
                    {
                        $rData = $action->insert();
                        echo $rData;
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function deleteSched($action) {
                        $rData = $action->remove();
                        echo $rData;
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function updateSched($action) {
                        $rData = $action->update();
                        echo $rData;
                        unset($action);
                    }


                    ?>
                    </tbody>
                </table>
                </form>
            </div>

            <!-- Begin Modal -->
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Update Schedule</h4>
                        </div>
                        <div class="modal-body">
                            <form id="updateForm" class="form-horizontal" method="POST" action="schedules.php">
                                <input type="hidden" name="mode" value="update">
                                <?php
                                if (isset($_GET["id"]) && !empty($_GET["id"]) && isset($_GET["mode"]) && !empty($_GET["mode"])) {
                                    $getID = $_GET['id'];
                                    $mode = $_GET['mode'];
                                }
                                if (isset($mode) && !empty($mode)) {
                                    if ($mode === "edit") {
                                        if (!($stmt = $mysqli->prepare("SELECT S.date, R.city, E.fname, E.lname, P.title, S.start_time, S.end_time, R.id, P.id,                                  S.id, S.employee_id
                                                                        FROM mby_schedule S INNER JOIN mby_employee E ON S.employee_id = E.id
                                                                        INNER JOIN mby_position P ON E.position_id = P.id
                                                                        INNER JOIN mby_restaurant R ON E.restaurant_id = R.id
                                                                        WHERE S.id = ?
                                                                        "))
                                        ) {
                                            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!($stmt->bind_param('i', $getID))) {
                                            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!$stmt->execute()) {
                                            echo "Execute failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }
                                        if (!$stmt->bind_result($date, $city, $fname, $lname, $title, $start, $end, $restaurant_id, $position_id, $id, $sched_eid)) {
                                            echo "Bind failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }

                                        $stmt->fetch();
                                        $stmt->close();


                                        echo "<input type=\"hidden\" name=\"id\" value=\"". $id ."\">

                                            <div class=\"form-group\">
                                                <h4 class=\"col-lg-6\">". $fname . " " . $lname . "</h4>
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Date:</label>
                                                <input id=\"datetimepicker-4\" type=\"text\" name=\"date\" value=\"". $date . "\">
                                            </div>

                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Start Time:</label>
                                                <input id=\"datetimepicker-5\" type=\"text\" name=\"starttime\" value=\"". $start . "\">
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >End Time:</label>
                                                <input id=\"datetimepicker-6\" type=\"text\" name=\"endtime\" value=\"". $end . "\">
                                            </div>";

                                    }

                                }

                                ?>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input name="submitEdit" value="Save" class="btn btn-primary" type="submit">
                        </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>

    <script>
        $(document).ready(function(){
            var queryString = window.location.search;
            queryString = queryString.substring(1);
            var params = parseQueryString(queryString);

            if (params["mode"] === "edit")
            {
                var filename = getFileName();

                console.log(params);
                console.log(filename);
                $('#myModal').modal('show');
            }

        });

        $('#eid').change(function() {
            $( "select#eid option:selected" ).each(function() {
                $( "#position" ).val($( this ).attr('data-position'));
                $( "#pid" ).val($( this ).attr('data-pid'));
                $( "#city" ).val($( this ).attr('data-city'));
                $( "#rid" ).val($( this ).attr('data-rid'));
                $( "#fname" ).val($( this ).attr('data-fname'));
                $( "#lname" ).val($( this ).attr('data-lname'));
            });

        });

        $("#position").css("background-image", "");


        $('#datetimepicker-1').datetimepicker({
            timepicker:false,
            format: 'Y-m-d',
            mask:true, // '2016/12/31
            minDate:'2016/03/01',//yesterday is minimum date(for today use 0 or -1970/01/01)
            maxDate:'2018/01/02',
            startDate: '2016/03/01'
        });
        $('#datetimepicker-2').datetimepicker({
            datepicker:false,
            format: 'H:i',
            mask:true
        });
        $('#datetimepicker-3').datetimepicker({
            datepicker:false,
            format: 'H:i',
            mask:true
        });
        $('#datetimepicker-4').datetimepicker({
            timepicker:false,
            format: 'Y-m-d',
            mask:true, // '2016/12/31
            minDate:'2016/03/01',//yesterday is minimum date(for today use 0 or -1970/01/01)
            maxDate:'2018/01/02',
            startDate: '2016/03/01'
        });
        $('#datetimepicker-5').datetimepicker({
            datepicker:false,
            format: 'H:i',
            mask:true
        });
        $('#datetimepicker-6').datetimepicker({
            datepicker:false,
            format: 'H:i',
            mask:true
        });

        function parseQueryString( queryString ) {
            var params = {}, queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for ( i = 0, l = queries.length; i < l; i++ ) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };

        function getFileName() {
            //this gets the full url
            var url = document.location.href;
            //this removes the anchor at the end, if there is one
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
            //this removes the query after the file name, if there is one
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
            //this removes everything before the last slash in the path
            url = url.substring(url.lastIndexOf("/") + 1, url.length);
            return url;
        }
    </script>

    </body>
    </html>
<?php
//close database connection
$mysqli->close();
?>