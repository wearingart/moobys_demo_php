<?php
/*
 * This page is used to display the menu items from the Moobys database. It uses the Moobys\Menu class to run the
 * main sql calls that add, insert, update, and delete data.
 *
 */

require 'Moobys/Menu.php';
use Moobys\Menu as mbyMenu;

ini_set('display_errors', 'On');


$page_id = "menu";

if(isset($_POST["id"])) {
    $id = $_POST["id"];
}

require 'connect.php';

$mysqli = db_connect();

$stmt = '';

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CS340 Project: Mooby's Food and Fun</title>

        <!-- Bootstrap core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">

    </head>

    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Mooby's Database Admin</a>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class=<?php $class_value = ($page_id == "home") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="index.php">Overview</a></li>
                    <li class=<?php $class_value = ($page_id == "restaurant") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="restaurants.php">Restaurants</a></li>
                    <li class=<?php $class_value = ($page_id == "employees") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="employees.php">Employees</a></li>
                    <li class=<?php $class_value = ($page_id == "positions") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="positions.php">Positions</a></li>
                    <li class=<?php $class_value = ($page_id == "menu") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="menu.php">Menu Items</a></li>
                    <li class=<?php $class_value = ($page_id == "schedule") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="schedules.php">Work Schedules</a> </li>
                    <li class=<?php $class_value = ($page_id == "sales") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="sales.php">Sales</a> </li>
                </ul>
                <ul class="nav nav-sidebar">
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1>Mooby's Menu</h1>

            <div id="formAddMenuItem" name="formAddMenuItem">
                <form class="form-horizontal" method="POST" action="menu.php">
                    <fieldset>
                        <legend>Enter Menu Item</legend>
                        <div class="form-group">
                            <label class="col-md-2" >Menu Item Name:</label>
                            <input class="col-md-3" type="text" name="name">
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" >Price:</label>
                            <input class="col-md-3" type="text" name="price">
                        </div>

                        <div class="form-group">
                            <label class="col-md-2" >Description:</label>
                            <textarea class="form-control" rows="4" cols="50" name="description" style="width: 380px;">Enter text here...</textarea>
                        </div>

                        <input type="hidden" name="mode" value="add">
                        <input name="submit" value="Add Menu Item" class="btn btn-primary" type="submit">
                    </fieldset>
                </form>

            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    //contains basic logic for delete, update, and setting mode
                    include "includes/logic.inc.php";
                    unset($params);
                    unset($data);

                    //used in insert and update functions. mode distinguishes which is used.
                    if (isset($_POST['name']) && !empty($_POST['name'])) {
                        $postName =         $_POST['name'];

                        if (isset($_POST['price']) && !empty($_POST['price'])) {
                            $postPrice = $_POST['price'];

                            if (isset($_POST['description']) && !empty($_POST['description'])) {
                                $postDescription = $_POST['description'];

                                $data = ['name'=>$postName, 'price'=>$postPrice, 'description'=>$postDescription];

                                $valueIsSet = true;
                            }
                        }
                    }


                    if (!isset($data)) {
                        $valueIsSet = false;
                        echo "NOT SET";
                    }


                    if ($modeSet) {
                        if ($mode === "add") {
                            $sql = 'INSERT INTO mby_menu_items(name, price, description) VALUES (?,?,?)';
                            $params = 'sds';
                            $action = new mbyMenu($data, $params, $sql, $mysqli);
                            insertMenuItem($action);

                            $sql = 'SELECT M.id, M.name, M.price, M.description FROM mby_menu_items M';
                            $params = array(['id','name','price','description']);
                            $action = new mbyMenu('', $params, $sql, $mysqli);
                            getMenu($action);
                            unset($action);
                        }
                        elseif ($mode === "delete") {
                            $sql = 'DELETE FROM mby_menu_items WHERE id = ?';
                            $params = 'i';
                            $action = new mbyMenu($getID, $params, $sql, $mysqli);
                            deleteMenuItem($action);

                            $sql = 'SELECT M.id, M.name, M.price, M.description FROM mby_menu_items M';
                            $params = array(['id','name','price','description']);
                            $action = new mbyMenu('', $params, $sql, $mysqli);
                            getMenu($action);
                            unset($action);
                        }
                        elseif ($mode === "edit") {
                            $sql = 'SELECT M.id, M.name, M.price, M.description FROM mby_menu_items M';
                            $params = array(['id','name','price','description']);
                            $action = new mbyMenu('', $params, $sql, $mysqli);
                            getMenu($action);
                            unset($action);
                        }
                        elseif ($mode === "update") {
                            unset($data);
                            $data = ['name'=>$postName, 'price'=>$postPrice, 'description'=>$postDescription, 'id'=>$postID];
                            $sql = 'UPDATE mby_menu_items SET name = ?, price = ?, description = ? WHERE id = ?';
                            $params = 'sdsi';
                            $action = new mbyMenu($data, $params, $sql, $mysqli);
                            updateMenu($action);

                            $sql = 'SELECT M.id, M.name, M.price, M.description FROM mby_menu_items M';
                            $params = array(['id','name','price','description']);
                            $action = new mbyMenu('', $params, $sql, $mysqli);
                            getMenu($action);
                            unset($action);
                        }
                    }
                    else {
                        $sql = 'SELECT M.id, M.name, M.price, M.description FROM mby_menu_items M';
                        $params = array(['id','name','price','description']);
                        $action = new mbyMenu('', $params, $sql, $mysqli);
                        getMenu($action);
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function getMenu($action)
                    {
                        $rData = $action->show();
                        unset($action);
                        echo $rData;
                    }

                    /**
                     * @param $action
                     */
                    function insertMenuItem($action)
                    {
                        $rData = $action->insert();
                        echo $rData;
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function deleteMenuItem($action) {
                        $rData = $action->remove();
                        echo $rData;
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function updateMenu($action) {
                        $rData = $action->update();
                        echo $rData;
                        unset($action);
                    }


                    ?>
                    </tbody>
                </table>
                </form>
            </div>

            <!-- Begin Modal -->
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Update Menu Items</h4>
                        </div>
                        <div class="modal-body">
                            <form id="updateForm" class="form-horizontal" method="POST" action="menu.php">
                                <input type="hidden" name="mode" value="update">
                                <?php
                                if (isset($_GET["id"]) && !empty($_GET["id"]) && isset($_GET["mode"]) && !empty($_GET["mode"])) {
                                    $getID = $_GET['id'];
                                    $mode = $_GET['mode'];
                                }
                                if (isset($mode) && !empty($mode)) {
                                    if ($mode === "edit") {
                                        if (!($stmt = $mysqli->prepare("SELECT M.id, M.name, M.price, M.description FROM mby_menu_items M WHERE id = ?"))
                                        ) {
                                            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!($stmt->bind_param('i', $getID))) {
                                            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!$stmt->execute()) {
                                            echo "Execute failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }
                                        if (!$stmt->bind_result($id, $name, $price, $description)) {
                                            echo "Bind failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }

                                        $stmt->fetch();
                                        $stmt->close();

                                        echo "<legend>Enter Menu Item</legend>
                                                <div class=\"form-group\">
                                                    <label class=\"col-md-2\" >Menu Item Name:</label>
                                                    <input class=\"col-md-3\" type=\"text\" name=\"name\" value=\"" . $name . "\">
                                                </div>
                                                <div class=\"form-group\">
                                                    <label class=\"col-md-2\" >Price:</label>
                                                    <input class=\"col-md-3\" type=\"text\" name=\"price\" value=\"" . $price . "\">                                                </div>

                                                <div class=\"form-group\">
                                                    <label class=\"col-md-2\" >Description:</label>
                                                    <textarea class=\"form-control\" rows=\"4\" cols=\"50\" name=\"description\" style=\"width: 380px;\">" . $description . "</textarea>
                                                </div>
                                                <input type=\"hidden\" name=\"id\" value=\"" . $id . "\">";
                                    }

                                }

                                ?>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input name="submitEdit" value="Save" class="btn btn-primary" type="submit">
                        </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function(){
            var queryString = window.location.search;
            queryString = queryString.substring(1);
            var params = parseQueryString(queryString);

            if (params["mode"] === "edit")
            {
                var filename = getFileName();

                console.log(params);
                console.log(filename);
                $('#myModal').modal('show');
            }

        });

        function parseQueryString( queryString ) {
            var params = {}, queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for ( i = 0, l = queries.length; i < l; i++ ) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };

        function getFileName() {
            //this gets the full url
            var url = document.location.href;
            //this removes the anchor at the end, if there is one
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
            //this removes the query after the file name, if there is one
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
            //this removes everything before the last slash in the path
            url = url.substring(url.lastIndexOf("/") + 1, url.length);
            return url;
        }
    </script>

    </body>
    </html>
<?php
//close database connection
$mysqli->close();
?>