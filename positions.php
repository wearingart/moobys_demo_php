<?php
/*
 * This page is used to display the positions from the Moobys database. It uses the Moobys\Positions class to run the
 * main sql calls that add, insert, update, and delete data.
 *
 */
require 'Moobys/Position.php';
use Moobys\Positions as mbyPositions;

ini_set('display_errors', 'On');


$page_id = "positions";

if(isset($_POST["id"])) {
    $id = $_POST["id"];
}

require 'connect.php';

$mysqli = db_connect();
$stmt = '';

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CS340 Project: Mooby's Food and Fun</title>

        <!-- Bootstrap core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">

    </head>

    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Mooby's Database Admin</a>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class=<?php $class_value = ($page_id == "home") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="index.php">Overview</a></li>
                    <li class=<?php $class_value = ($page_id == "restaurant") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="restaurants.php">Restaurants</a></li>
                    <li class=<?php $class_value = ($page_id == "employees") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="employees.php">Employees</a></li>
                    <li class=<?php $class_value = ($page_id == "positions") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="positions.php">Positions</a></li>
                    <li class=<?php $class_value = ($page_id == "menu") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="menu.php">Menu Items</a></li>
                    <li class=<?php $class_value = ($page_id == "schedule") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="schedules.php">Work Schedules</a> </li>
                    <li class=<?php $class_value = ($page_id == "sales") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="sales.php">Sales</a> </li>
                </ul>
                <ul class="nav nav-sidebar">
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1>Mooby's Positions</h1>
                <div id="formAddPosition" name="formAddPosition">
                    <form class="form-horizontal" method="POST" action="positions.php">
                        <fieldset>
                            <legend>Enter New Position:</legend>
                            <div class="form-group">
                                <label class="col-sm-1" >Title:</label>
                                <input class="col-md-2" type="text" name="title" value="Add New Position">
                            </div>
                            <input type="hidden" name="mode" value="add">
                            <input name="submit" value="Add Position" class="btn btn-primary" type="submit">
                        </fieldset>
                    </form>
                </div>

                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Title</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        //contains basic logic for delete, update, and setting mode
                        include "includes/logic.inc.php";
                        unset($params);
                        unset($data);

                        //used in insert and update functions. mode distinguishes which is used.
                        if (isset($_POST['title']) && !empty($_POST['title'])) {
                            $postTitle = $_POST['title'];
                            $data = ['title'=>$postTitle];
                            $valueIsSet = true;
                        }
                        else {
                            $valueIsSet = false;
                        }


                        if ($modeSet) {
                            if ($mode === "add") {
                                $sql = 'INSERT INTO mby_position(title) VALUES (?)';
                                $params = 's';
                                $action = new mbyPositions($data, $params, $sql, $mysqli);
                                insertRestaurant($action);

                                $sql = 'SELECT P.id, P.title FROM mby_position P';
                                $params = array(['id','address','city','state','zip']);
                                $action = new mbyPositions('', $params, $sql, $mysqli);
                                getRestaurants($action);
                                unset($action);
                            }
                            elseif ($mode === "delete") {
                                $sql = 'DELETE FROM mby_position WHERE id = ?';
                                $params = 'i';
                                $action = new mbyPositions($getID, $params, $sql, $mysqli);
                                deleteRestaurant($action);

                                $sql = 'SELECT P.id, P.title FROM mby_position P';
                                $params = array(['id','address','city','state','zip']);
                                $action = new mbyPositions('', $params, $sql, $mysqli);
                                getRestaurants($action);
                                unset($action);
                            }
                            elseif ($mode === "edit") {
                                $sql = 'SELECT P.id, P.title FROM mby_position P';
                                $params = array(['id','address','city','state','zip']);
                                $action = new mbyPositions('', $params, $sql, $mysqli);
                                getRestaurants($action);
                                unset($action);
                            }
                            elseif ($mode === "update") {
                                unset($data);
                                $data = ['title'=>$postTitle, 'id'=>$postID];
                                $sql = 'UPDATE mby_position SET title = ? WHERE id = ?';
                                $params = 'si';
                                $action = new mbyPositions($data, $params, $sql, $mysqli);
                                updateRestaurant($action);

                                $sql = 'SELECT P.id, P.title FROM mby_position P';
                                $params = array(['id','address','city','state','zip']);
                                $action = new mbyPositions('', $params, $sql, $mysqli);
                                getRestaurants($action);
                                unset($action);
                            }
                        }
                        else {
                            $sql = 'SELECT P.id, P.title FROM mby_position P';
                            $params = array(['id','address','city','state','zip']);
                            $action = new mbyPositions('', $params, $sql, $mysqli);
                            getRestaurants($action);
                            unset($action);
                        }

                        /**
                         * @param $action
                         */
                        function getRestaurants($action)
                        {
                            $rData = $action->show();
                            unset($action);
                            echo $rData;
                        }

                        /**
                         * @param $action
                         */
                        function insertRestaurant($action)
                        {
                            $rData = $action->insert();
                            echo $rData;
                            unset($action);
                        }


                        /**
                         * @param $action
                         */
                        function deleteRestaurant($action) {
                            $rData = $action->remove();
                            echo $rData;
                            unset($action);
                        }

                        /**
                         * @param $action
                         */
                        function updateRestaurant($action) {
                            $rData = $action->update();
                            echo $rData;
                            unset($action);
                        }


                        ?>
                        </tbody>
                    </table>
                    </form>
                </div>

                <!-- Begin Modal -->
                <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Update Position</h4>
                            </div>
                            <div class="modal-body">
                                <form id="updateForm" class="form-horizontal" method="POST" action="positions.php">
                                    <input type="hidden" name="mode" value="update">
                                    <?php
                                    if (isset($_GET["id"]) && !empty($_GET["id"]) && isset($_GET["mode"]) && !empty($_GET["mode"])) {
                                        $getID = $_GET['id'];
                                        $mode = $_GET['mode'];
                                    }
                                    if (isset($mode) && !empty($mode)) {
                                        if ($mode === "edit") {
                                            if (!($stmt = $mysqli->prepare("SELECT P.id, P.title FROM mby_position P WHERE id = ?"))) {
                                                echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                            }
                                            if (!($stmt->bind_param('i', $getID))) {
                                                echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
                                            }
                                            if (!$stmt->execute()) {
                                                echo "Execute failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                            }
                                            if (!$stmt->bind_result($id, $title)) {
                                                echo "Bind failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                            }

                                            $stmt->fetch();

                                            echo "<input type=\"hidden\" name=\"id\" value=\"". $id ."\">
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Title:</label>
                                                <input class=\"col-md-4\" type=\"text\" name=\"title\" value=\"". $title ."\">
                                            </div>";
                                            $stmt->close();
                                        }
                                    }

                                    ?>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <input name="submitEdit" value="Save" class="btn btn-primary" type="submit">
                            </div>
                            </form>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>
        </div>


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function(){
                var queryString = window.location.search;
                queryString = queryString.substring(1);
                var params = parseQueryString(queryString);

                if (params["mode"] === "edit")
                {
                    var filename = getFileName();

                    console.log(params);
                    console.log(filename);
                    $('#myModal').modal('show');
                }

            });

            function parseQueryString( queryString ) {
                var params = {}, queries, temp, i, l;

                // Split into key/value pairs
                queries = queryString.split("&");

                // Convert the array of strings into an object
                for ( i = 0, l = queries.length; i < l; i++ ) {
                    temp = queries[i].split('=');
                    params[temp[0]] = temp[1];
                }

                return params;
            };

            function getFileName() {
                //this gets the full url
                var url = document.location.href;
                //this removes the anchor at the end, if there is one
                url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
                //this removes the query after the file name, if there is one
                url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
                //this removes everything before the last slash in the path
                url = url.substring(url.lastIndexOf("/") + 1, url.length);
                return url;
            }
        </script>

    </body>
    </html>
<?php
//close database connection
$mysqli->close();
?>