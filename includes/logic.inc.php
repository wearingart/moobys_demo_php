<?php

$valueIsSet = false;
$modeSet = false;

//used in delete function
if (isset($_GET["id"]) && !empty($_GET["id"]) && isset($_GET["mode"]) && !empty($_GET["mode"])) {
    $getID = $_GET['id'];
    $mode = $_GET['mode'];
    $modeSet = true;
}
//used in update function
elseif (isset($_POST["id"]) && !empty($_POST["id"]) && isset($_POST["mode"]) && !empty($_POST["mode"])) {
    $postID = $_POST['id'];
    $mode = $_POST['mode'];
    $modeSet = true;
}
//used in $modeSet if statement
elseif (isset($_POST["mode"]) && !empty($_POST["mode"])) {
    $mode = $_POST['mode'];
    $modeSet = true;
}
else {
    $modeSet = false;
}