<?php
/*
 * This page is used to display the restaurants from the Moobys database. It uses the Moobys\Restaurants class to run the
 * main sql calls that add, insert, update, and delete data.
 *
 */

require 'Moobys/Restaurant.php';
use Moobys\Restaurant as mbyRestaurant;

ini_set('display_errors', 'On');


$page_id = "restaurant";

if(isset($_POST["id"])) {
    $id = $_POST["id"];
}

require 'connect.php';

$mysqli = db_connect();
$stmt = '';

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CS340 Project: Mooby's Food and Fun</title>

        <!-- Bootstrap core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">

    </head>

    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Mooby's Database Admin</a>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class=<?php $class_value = ($page_id == "home") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="index.php">Overview</a></li>
                    <li class=<?php $class_value = ($page_id == "restaurant") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="restaurants.php">Restaurants</a></li>
                    <li class=<?php $class_value = ($page_id == "employees") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="employees.php">Employees</a></li>
                    <li class=<?php $class_value = ($page_id == "positions") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="positions.php">Positions</a></li>
                    <li class=<?php $class_value = ($page_id == "menu") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="menu.php">Menu Items</a></li>
                    <li class=<?php $class_value = ($page_id == "schedule") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="schedules.php">Work Schedules</a> </li>
                    <li class=<?php $class_value = ($page_id == "sales") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="sales.php">Sales</a> </li>
                </ul>
                <ul class="nav nav-sidebar">
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1>Mooby's Restaurants</h1>
            <div id="formAddRest" name="formAddRest">
                <form class="form-horizontal" method="POST" action="restaurants.php">
                    <fieldset>
                        <legend>Enter New Location Address</legend>
                        <div class="form-group">
                            <label class="col-md-1" >Address:</label>
                            <input class="col-md-3" type="text" name="address">
                        </div>
                        <div class="form-group">
                            <label class="col-md-1" >City:</label>
                            <input class="col-md-3" type="text" name="city">
                        </div>
                        <div class="form-group">
                            <label class="col-md-1" >State:</label>

                            <select class="col-md-3" name="state">
                                <option value="">Choose...</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-md-1" >Zip:</label>
                            <input class="col-md-3" type="text" name="zip">
                        </div>
                        <input type="hidden" name="mode" value="add">
                        <input name="submit" value="Add Restaurant" class="btn btn-primary" type="submit">
                    </fieldset>
                </form>

            </div>

            <div class="table-responsive">
                <form id="restDataForm">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>ID</th>
                            <th>Address</th>
                            <th>City</th>
                            <th>State</th>
                            <th>Zip</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php
                        //contains basic logic for delete, update, and setting mode
                        include "includes/logic.inc.php";
                        unset($params);
                        unset($data);

                        //used in insert and update functions. mode distinguishes which is used.
                        if (isset($_POST['address']) && !empty($_POST['address']) && isset($_POST['city']) && !empty($_POST['city']) && isset($_POST['state']) && !empty($_POST['state']) && isset($_POST['zip']) && !empty($_POST['zip'])) {
                            $postAddress = $_POST['address'];
                            $postCity = $_POST['city'];
                            $postState = $_POST['state'];
                            $postZip = $_POST['zip'];
                            $data = ['address'=>$postAddress, 'city'=>$postCity, 'state'=>$postState, 'zip'=>$postZip];

                            $valueIsSet = true;
                        }
                        else {
                            $valueIsSet = false;
                        }


                        if ($modeSet) {
                            if ($mode === "add") {
                                $sql = 'INSERT INTO mby_restaurant(address, city, state, zip) VALUES (?,?,?,?)';
                                $params = 'ssss';
                                $action = new mbyRestaurant($data, $params, $sql, $mysqli);
                                insertRestaurant($action);

                                $sql = 'SELECT id, address, city, state, zip FROM mby_restaurant';
                                $params = array(['id','address','city','state','zip']);
                                $action = new mbyRestaurant('', $params, $sql, $mysqli);
                                getRestaurants($action);
                                unset($action);
                            }
                            elseif ($mode === "delete") {
                                $sql = 'DELETE FROM mby_restaurant WHERE id = ?';
                                $params = 'i';
                                $action = new mbyRestaurant($getID, $params, $sql, $mysqli);
                                deleteRestaurant($action);

                                $sql = 'SELECT id, address, city, state, zip FROM mby_restaurant';
                                $params = array(['id','address','city','state','zip']);
                                $action = new mbyRestaurant('', $params, $sql, $mysqli);
                                getRestaurants($action);
                                unset($action);
                            }
                            elseif ($mode === "edit") {
                                $sql = 'SELECT id, address, city, state, zip FROM mby_restaurant';
                                $params = array(['id','address','city','state','zip']);
                                $action = new mbyRestaurant('', $params, $sql, $mysqli);
                                getRestaurants($action);
                                unset($action);
                            }
                            elseif ($mode === "update") {
                                unset($data);
                                $data = ['address'=>$postAddress, 'city'=>$postCity, 'state'=>$postState, 'zip'=>$postZip, 'id'=>$postID];
                                $sql = 'UPDATE mby_restaurant SET address = ?, city = ?, state = ?, zip = ? WHERE id = ?';
                                $params = 'ssssi';
                                $action = new mbyRestaurant($data, $params, $sql, $mysqli);
                                updateRestaurant($action);

                                $sql = 'SELECT id, address, city, state, zip FROM mby_restaurant';
                                $params = array(['id','address','city','state','zip']);
                                $action = new mbyRestaurant('', $params, $sql, $mysqli);
                                getRestaurants($action);
                                unset($action);
                            }
                        }
                        else {
                            $sql = 'SELECT id, address, city, state, zip FROM mby_restaurant';
                            $params = array(['id','address','city','state','zip']);
                            $action = new mbyRestaurant('', $params, $sql, $mysqli);
                            getRestaurants($action);
                            unset($action);
                        }

                        /**
                         * @param $action
                         */
                        function getRestaurants($action)
                        {
                            $rData = $action->show();
                            unset($action);
                            echo $rData;
                        }

                        /**
                         * @param $action
                         */
                        function insertRestaurant($action)
                        {
                            $rData = $action->insert();
                            echo $rData;
                            unset($action);
                        }

                        /**
                         * @param $action
                         */
                        function deleteRestaurant($action) {
                            $rData = $action->remove();
                            echo $rData;
                            unset($action);
                        }

                        /**
                         * @param $action
                         */
                        function updateRestaurant($action) {
                            $rData = $action->update();
                            echo $rData;
                            unset($action);
                        }


                        ?>
                        </tbody>
                    </table>
                </form>
            </div>

            <!-- Begin Modal -->
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Update Restaurant Location</h4>
                        </div>
                        <div class="modal-body">
                            <form id="updateForm" class="form-horizontal" method="POST" action="restaurants.php">
                                <input type="hidden" name="mode" value="update">
                                <?php
                                if (isset($_GET["id"]) && !empty($_GET["id"]) && isset($_GET["mode"]) && !empty($_GET["mode"])) {
                                    $getID = $_GET['id'];
                                    $mode = $_GET['mode'];
                                }
                                if (isset($mode) && !empty($mode)) {
                                    if ($mode === "edit") {
                                        if (!($stmt = $mysqli->prepare("SELECT id, address, city, state, zip FROM mby_restaurant WHERE id = ?"))) {
                                            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!($stmt->bind_param('i', $getID))) {
                                            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!$stmt->execute()) {
                                            echo "Execute failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }
                                        if (!$stmt->bind_result($id, $address, $city, $state, $zip)) {
                                            echo "Bind failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }

                                        $stmt->fetch();

                                        echo "<input type=\"hidden\" name=\"id\" value=\"". $id ."\">
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Address:</label>
                                                <input class=\"col-md-4\" type=\"text\" name=\"address\" value=\"". $address ."\">
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >City:</label>
                                                <input class=\"col-md-4\" type=\"text\" name=\"city\" value=\"". $city ."\">
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >State:</label>
                                                <select class=\"col-md-4\" name=\"state\">
                                                    <option value=\"\">Choose...</option>";

                                        $statesArr = array(	'AL'=>'ALABAMA', 'AK'=>'ALASKA', 'AS'=>'AMERICAN SAMOA', 'AZ'=>'ARIZONA',
                                            'AR'=>'ARKANSAS', 'CA'=>'CALIFORNIA', 'CO'=>'COLORADO', 'CT'=>'CONNECTICUT', 'DE'=>'DELAWARE',
                                            'DC'=>'DISTRICT OF COLUMBIA', 'FM'=>'FEDERATED STATES OF MICRONESIA', 'FL'=>'FLORIDA', 'GA'=>'GEORGIA',
                                            'HI'=>'HAWAII', 'ID'=>'IDAHO', 'IL'=>'ILLINOIS', 'IN'=>'INDIANA', 'IA'=>'IOWA', 'KS'=>'KANSAS',
                                            'KY'=>'KENTUCKY', 'LA'=>'LOUISIANA', 'ME'=>'MAINE', 'MH'=>'MARSHALL ISLANDS', 'MD'=>'MARYLAND',
                                            'MA'=>'MASSACHUSETTS', 'MI'=>'MICHIGAN', 'MN'=>'MINNESOTA', 'MS'=>'MISSISSIPPI', 'MO'=>'MISSOURI',
                                            'MT'=>'MONTANA', 'NE'=>'NEBRASKA', 'NV'=>'NEVADA', 'NH'=>'NEW HAMPSHIRE', 'NJ'=>'NEW JERSEY',
                                            'NM'=>'NEW MEXICO', 'NY'=>'NEW YORK', 'NC'=>'NORTH CAROLINA', 'ND'=>'NORTH DAKOTA',
                                            'MP'=>'NORTHERN MARIANA ISLANDS', 'OH'=>'OHIO', 'OK'=>'OKLAHOMA', 'OR'=>'OREGON',
                                            'PA'=>'PENNSYLVANIA', 'RI'=>'RHODE ISLAND', 'SC'=>'SOUTH CAROLINA', 'SD'=>'SOUTH DAKOTA',
                                            'TN'=>'TENNESSEE', 'TX'=>'TEXAS', 'UT'=>'UTAH', 'VT'=>'VERMONT', 'VA'=>'VIRGINIA',
                                            'WA'=>'WASHINGTON', 'WV'=>'WEST VIRGINIA', 'WI'=>'WISCONSIN', 'WY'=>'WYOMING');
                                        foreach ($statesArr as $key=>$value) {
                                            if ($state !== $key) {
                                                echo "<option value=\"" . $key . "\">" . $value . "</option>";
                                            }
                                            else {
                                                echo "<option value=\"" . $key . "\" selected>" . $value . "</option>";
                                            }
                                        }


                                        echo "</select>
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Zip:</label>
                                                <input class=\"col-md-4\" type=\"text\" name=\"zip\" pattern=\"\\d{5}\" value=\"" . $zip . "\">
                                            </div>";
                                        $stmt->close();
                                    }
                                }

                                ?>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input name="submitEdit" value="Save" class="btn btn-primary" type="submit">
                        </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function(){
            var queryString = window.location.search;
            queryString = queryString.substring(1);
            var params = parseQueryString(queryString);

            if (params["mode"] === "edit")
            {
                var filename = getFileName();

                console.log(params);
                console.log(filename);
                $('#myModal').modal('show');
            }

        });

        function parseQueryString( queryString ) {
            var params = {}, queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for ( i = 0, l = queries.length; i < l; i++ ) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };

        function getFileName() {
            //this gets the full url
            var url = document.location.href;
            //this removes the anchor at the end, if there is one
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
            //this removes the query after the file name, if there is one
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
            //this removes everything before the last slash in the path
            url = url.substring(url.lastIndexOf("/") + 1, url.length);
            return url;
        }
    </script>

    </body>
    </html>
<?php
//close database connection
$mysqli->close();
?>