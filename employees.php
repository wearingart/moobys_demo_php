<?php
/*
 * This page is used to display the employees from the Moobys database. It uses the Moobys\Employees class to run the
 * main sql calls that add, insert, update, and delete data.
 *
 */
require 'Moobys/Employee.php';
use Moobys\Employees as mbyEmployee;

ini_set('display_errors', 'On');


$page_id = "employees";

if(isset($_POST["id"])) {
    $id = $_POST["id"];
}

require 'connect.php';

$mysqli = db_connect();

$stmt = '';

?>
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CS340 Project: Mooby's Food and Fun</title>

        <!-- Bootstrap core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">

    </head>

    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Mooby's Database Admin</a>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class=<?php $class_value = ($page_id == "home") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="index.php">Overview</a></li>
                    <li class=<?php $class_value = ($page_id == "restaurant") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="restaurants.php">Restaurants</a></li>
                    <li class=<?php $class_value = ($page_id == "employees") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="employees.php">Employees</a></li>
                    <li class=<?php $class_value = ($page_id == "positions") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="positions.php">Positions</a></li>
                    <li class=<?php $class_value = ($page_id == "menu") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="menu.php">Menu Items</a></li>
                    <li class=<?php $class_value = ($page_id == "schedule") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="schedules.php">Work Schedules</a> </li>
                    <li class=<?php $class_value = ($page_id == "sales") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="sales.php">Sales</a> </li>
                </ul>
                <ul class="nav nav-sidebar">
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1>Mooby's Employees</h1>

            <!-- Form used to add an employee to the database -->
            <div id="formAddEmployee" name="formAddEmployee">
                <form class="form-horizontal" method="POST" action="employees.php">
                    <fieldset>
                        <legend>Enter Employee</legend>
                        <div class="form-group">
                            <label class="col-md-2" >First Name:</label>
                            <input class="col-md-3" type="text" name="fname">
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" >Last Name:</label>
                            <input class="col-md-3" type="text" name="lname">
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" >Position:</label>
                            <select class="col-md-3" name="position">
                                <option value="" hidden>Choose...</option>
                                <?php
                                //mysqli function calls to generate select options of positions
                                if (!($stmt = $mysqli->prepare('SELECT P.id, P.title FROM mby_position P ORDER BY P.title ASC'))) {
                                    echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                }

                                if (!$stmt->execute()) {
                                    echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                if (!$stmt->bind_result($id, $title)) {
                                    echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                while ($stmt->fetch()) {
                                    echo "<option value=\"" . $id . "\">" . $title . "</option>";
                                }
                                $stmt->close();

                                ?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="col-md-2" >Restaurant Location:</label>
                            <select class="col-md-3" name="restaurant">
                                <option value="" hidden>Choose...</option>
                                <?php
                                //mysqli function calls to generate select options of restaurant locations
                                if (!($stmt = $mysqli->prepare('SELECT id, address, city, state FROM mby_restaurant ORDER BY state ASC, city ASC'))) {
                                    echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                }

                                if (!$stmt->execute()) {
                                    echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                if (!$stmt->bind_result($id, $address, $city, $state)) {
                                    echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                while ($stmt->fetch()) {
                                    echo "<option value=\"" . $id . "\">" . $state . " - " . $city . " - " . $address . "</option>";
                                }
                                $stmt->close();

                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" >Hourly Rate:</label>
                            <input class="col-md-3" type="text" name="hourly">
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" >Phone Number:</label>
                            <input class="col-md-3" type="text" name="phone">
                        </div>

                        <input type="hidden" name="mode" value="add">
                        <input name="submit" value="Add Employee" class="btn btn-primary" type="submit">
                    </fieldset>
                </form>

            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>ID</th>
                        <th>Name</th>
                        <th></th>
                        <th>Position</th>
                        <th>Location</th>
                        <th>Hourly Rate</th>
                        <th>Phone #</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    //contains basic logic for delete, update, and setting mode
                    include "includes/logic.inc.php";
                    unset($params);
                    unset($data);

                    //used in insert and update functions. mode distinguishes which is used.
                    if (isset($_POST['fname']) && !empty($_POST['fname']) && isset($_POST['lname']) && !empty($_POST['lname']) && isset($_POST['position']) && !empty($_POST['position']) && isset($_POST['restaurant']) && !empty($_POST['restaurant']) && isset($_POST['hourly']) && !empty($_POST['hourly']) && isset($_POST['phone']) && !empty($_POST['phone'])) {

                        //set variables to POST data from forms
                        $postFirstName =    $_POST['fname'];
                        $postLastName =     $_POST['lname'];
                        $postPosition =     $_POST['position'];
                        $postHourly =       $_POST['hourly'];
                        $postPhone =        $_POST['phone'];
                        $postRestaurant =   $_POST['restaurant'];

                        $data = ['restaurant'=>$postRestaurant, 'position'=>$postPosition, 'fname'=>$postFirstName, 'lname'=>$postLastName, 'hourly'=>$postHourly, 'phone'=>$postPhone];

                        $valueIsSet = true;
                    }
                    else {
                        $valueIsSet = false;
                    }

                    //runs functions to insert, add, delete, and update depending on the mode
                    if ($modeSet) {
                        if ($mode === "add") {
                            //prepare variables to be passed to function to add a row
                            $sql = 'INSERT INTO mby_employee(restaurant_id, position_id, fname, lname, hourly_rate, phone) VALUES (?,?,?,?,?,?)';
                            $params = 'iissds';
                            $action = new mbyEmployee($data, $params, $sql, $mysqli);
                            insertEmployee($action);

                            //call show function to show updated table
                            $sql = 'SELECT E.id, E.fname, E.lname, P.title, R.city, E.hourly_rate, E.phone, R.id, P.id
                                        FROM mby_employee E INNER JOIN mby_position P
                                        ON E.position_id = P.id INNER JOIN mby_restaurant R
                                        ON E.restaurant_id = R.id
                                        ORDER BY E.id ASC';
                            $params = array(['id','fname','lname','position','restaurant', 'hourly', 'phone', 'rid', 'pid']);
                            $action = new mbyEmployee('', $params, $sql, $mysqli);
                            getEmployee($action);
                            unset($action);
                        }
                        elseif ($mode === "delete") {
                            $sql = 'DELETE FROM mby_employee WHERE id = ?';
                            $params = 'i';
                            $action = new mbyEmployee($getID, $params, $sql, $mysqli);
                            deleteEmployee($action);

                            $sql = 'SELECT E.id, E.fname, E.lname, P.title, R.city, E.hourly_rate, E.phone, R.id, P.id
                                        FROM mby_employee E INNER JOIN mby_position P
                                        ON E.position_id = P.id INNER JOIN mby_restaurant R
                                        ON E.restaurant_id = R.id
                                        ORDER BY E.id ASC';
                            $params = array(['id','fname','lname','position','restaurant', 'hourly', 'phone', 'rid', 'pid']);
                            $action = new mbyEmployee('', $params, $sql, $mysqli);
                            getEmployee($action);
                            unset($action);
                        }
                        elseif ($mode === "edit") {
                            $sql = 'SELECT E.id, E.fname, E.lname, P.title, R.city, E.hourly_rate, E.phone, R.id, P.id
                                        FROM mby_employee E INNER JOIN mby_position P
                                        ON E.position_id = P.id INNER JOIN mby_restaurant R
                                        ON E.restaurant_id = R.id
                                        ORDER BY E.id ASC';
                            $params = array(['id','fname','lname','position','restaurant', 'hourly', 'phone', 'rid', 'pid']);
                            $action = new mbyEmployee('', $params, $sql, $mysqli);
                            getEmployee($action);
                            unset($action);
                        }
                        elseif ($mode === "update") {
                            unset($data);
                            $data = ['restaurant'=>$postRestaurant, 'position'=>$postPosition, 'fname'=>$postFirstName, 'lname'=>$postLastName, 'hourly'=>$postHourly
                                , 'phone'=>$postPhone, 'id'=>$postID];
                            $sql = 'UPDATE mby_employee SET restaurant_id = ?, position_id = ?, fname = ?, lname = ?, hourly_rate = ?, phone = ? WHERE id = ?';
                            $params = 'iissdsi';
                            printf("Host info: %s\n", $mysqli->host_info);
                            $action = new mbyEmployee($data, $params, $sql, $mysqli);
                            updateEmployee($action);

                            $sql = 'SELECT E.id, E.fname, E.lname, P.title, R.city, E.hourly_rate, E.phone, R.id, P.id
                                        FROM mby_employee E INNER JOIN mby_position P
                                        ON E.position_id = P.id INNER JOIN mby_restaurant R
                                        ON E.restaurant_id = R.id
                                        ORDER BY E.id ASC';
                            $params = array(['id','fname','lname','position','restaurant', 'hourly', 'phone', 'rid', 'pid']);
                            $action = new mbyEmployee('', $params, $sql, $mysqli);
                            getEmployee($action);
                            unset($action);
                        }
                    }
                    else {
                        $sql = 'SELECT E.id, E.fname, E.lname, P.title, R.city, E.hourly_rate, E.phone, R.id, P.id
                                    FROM mby_employee E INNER JOIN mby_position P
                                    ON E.position_id = P.id INNER JOIN mby_restaurant R
                                    ON E.restaurant_id = R.id
                                    ORDER BY E.id ASC';
                        $params = array(['id','fname','lname','position','restaurant', 'hourly', 'phone', 'rid', 'pid']);
                        $action = new mbyEmployee('', $params, $sql, $mysqli);
                        getEmployee($action);
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function getEmployee($action)
                    {
                        $rData = $action->show();
                        unset($action);
                        echo $rData;
                    }

                    /**
                     * @param $action
                     */
                    function insertEmployee($action)
                    {
                        $rData = $action->insert();
                        echo $rData;
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function deleteEmployee($action) {
                        $rData = $action->remove();
                        echo $rData;
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function updateEmployee($action) {
                        $rData = $action->update();
                        echo $rData;
                        unset($action);
                    }


                    ?>
                    </tbody>
                </table>
                </form>
            </div>

            <!-- Begin Modal that is called when updating a record -->
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Update Restaurant Location</h4>
                        </div>
                        <div class="modal-body">
                            <form id="updateForm" class="form-horizontal" method="POST" action="employees.php">
                                <input type="hidden" name="mode" value="update">
                                <?php
                                if (isset($_GET["id"]) && !empty($_GET["id"]) && isset($_GET["mode"]) && !empty($_GET["mode"])) {
                                    $getID = $_GET['id'];
                                    $mode = $_GET['mode'];
                                }
                                if (isset($mode) && !empty($mode)) {
                                    if ($mode === "edit") {
                                        if (!($stmt = $mysqli->prepare("SELECT E.id, E.fname, E.lname, P.title, R.city, E.hourly_rate, E.phone, R.id, P.id
                                                                        FROM mby_employee E INNER JOIN mby_position P
                                                                        ON E.position_id = P.id INNER JOIN mby_restaurant R
                                                                        ON E.restaurant_id = R.id
                                                                        WHERE E.id = ?
                                                                        ORDER BY E.id ASC"))
                                        ) {
                                            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!($stmt->bind_param('i', $getID))) {
                                            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!$stmt->execute()) {
                                            echo "Execute failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }
                                        if (!$stmt->bind_result($id, $fname, $lname, $title, $city, $hourly, $phone, $restaurant_id, $position_id)) {
                                            echo "Bind failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }

                                        $stmt->fetch();
                                        $stmt->close();

                                        echo "<input type=\"hidden\" name=\"id\" value=\"". $id ."\">
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >First Name:</label>
                                                <input class=\"col-md-3\" type=\"text\" name=\"fname\" value=\"" . $fname . "\">
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Last Name:</label>
                                                <input class=\"col-md-3\" type=\"text\" name=\"lname\" value=\"" . $lname . "\">
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Position:</label>
                                                <select class=\"col-md-3\" name=\"position\">
                                                    <option value=\"\">Choose...</option>";

                                                    if (!($stmt = $mysqli->prepare('SELECT P.id, P.title FROM mby_position P ORDER BY P.title ASC'))) {
                                                        echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                                    }

                                                    if (!$stmt->execute()) {
                                                        echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                                    }

                                                    if (!$stmt->bind_result($pid, $ptitle)) {
                                                        echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                                    }

                                                    while ($stmt->fetch()) {
                                                        if ($pid !== $position_id) {
                                                            echo "<option value=\"" . $pid . "\">" . $ptitle . "</option>";
                                                        }
                                                        else {
                                                            echo "<option value=\"" . $pid . "\" selected>" . $ptitle . "</option>";
                                                        }

                                                    }
                                                    $stmt->close();

                                        echo "  </select>
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Restaurant Location:</label>
                                                <select class=\"col-md-3\" name=\"restaurant\">
                                                    <option value=\"\">Choose...</option>";
                                                    if (!($stmt = $mysqli->prepare('SELECT id, address, city, state FROM mby_restaurant ORDER BY state ASC, city ASC'))) {
                                                        echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                                    }

                                                    if (!$stmt->execute()) {
                                                        echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                                    }

                                                    if (!$stmt->bind_result($rid, $raddress, $rcity, $rstate)) {
                                                        echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                                    }

                                                    while ($stmt->fetch()) {
                                                        if ($rid !== $restaurant_id) {
                                                            echo "<option value=\"" . $rid . "\">" . $rstate . " - " . $rcity . " - " . $raddress . "</option>";
                                                        }
                                                        else {
                                                            echo "<option value=\"" . $rid . "\" selected>" . $rstate . " - " . $rcity . " - " . $raddress . "</option>";
                                                        }
                                                    }
                                                    $stmt->close();

                                        echo "</select>
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Hourly Rate:</label>
                                                <input class=\"col-md-3\" type=\"text\" name=\"hourly\" value=\"" . $hourly . "\">
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-2\" >Phone Number:</label>
                                                <input class=\"col-md-3\" type=\"text\" name=\"phone\" value=\"" . $phone . "\">
                                            </div>";

                                    }

                                }

                                ?>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input name="submitEdit" value="Save" class="btn btn-primary" type="submit">
                        </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script>
        $(document).ready(function(){
            var queryString = window.location.search;
            queryString = queryString.substring(1);
            var params = parseQueryString(queryString);

            if (params["mode"] === "edit")
            {
                var filename = getFileName();

                console.log(params);
                console.log(filename);
                $('#myModal').modal('show');
            }

        });

        function parseQueryString( queryString ) {
            var params = {}, queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for ( i = 0, l = queries.length; i < l; i++ ) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };

        function getFileName() {
            //this gets the full url
            var url = document.location.href;
            //this removes the anchor at the end, if there is one
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
            //this removes the query after the file name, if there is one
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
            //this removes everything before the last slash in the path
            url = url.substring(url.lastIndexOf("/") + 1, url.length);
            return url;
        }
    </script>

    </body>
    </html>
<?php
//close database connection
$mysqli->close();
?>