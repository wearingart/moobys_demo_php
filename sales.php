<?php
require 'Moobys/Sale.php';
use Moobys\Sale as mbySale;

ini_set('display_errors', 'On');


$page_id = "sales";

if(isset($_POST["id"])) {
    $id = $_POST["id"];
}

require 'connect.php';

$mysqli = db_connect();

$stmt = '';

?>
    <!DOCTYPE html>
    <html lang="en" xmlns="http://www.w3.org/1999/html">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">

        <title>CS340 Project: Mooby's Food and Fun</title>

        <!-- Bootstrap core CSS -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="bower_components/datetimepicker/jquery.datetimepicker.css"/ >

        <!-- Custom styles for this template -->
        <link href="css/dashboard.css" rel="stylesheet">

    </head>

    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">Mooby's Database Admin</a>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-3 col-md-2 sidebar">
                <ul class="nav nav-sidebar">
                    <li class=<?php $class_value = ($page_id == "home") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="index.php">Overview</a></li>
                    <li class=<?php $class_value = ($page_id == "restaurant") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="restaurants.php">Restaurants</a></li>
                    <li class=<?php $class_value = ($page_id == "employees") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="employees.php">Employees</a></li>
                    <li class=<?php $class_value = ($page_id == "positions") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="positions.php">Positions</a></li>
                    <li class=<?php $class_value = ($page_id == "menu") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="menu.php">Menu Items</a></li>
                    <li class=<?php $class_value = ($page_id == "schedule") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="schedules.php">Work Schedules</a> </li>
                    <li class=<?php $class_value = ($page_id == "sales") ? "\"active\"" : "\"\""; echo $class_value; ?>><a href="sales.php">Sales</a> </li>
                </ul>
                <ul class="nav nav-sidebar">
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1>Mooby's Employee Schedules</h1>
            <div id="test"></div>
            <div id="formAddSchedule" name="formAddSchedule">
                <form class="form-horizontal" method="POST" action="sales.php">
                    <fieldset>
                        <legend>Enter Sales</legend>
                        <div class="form-group">
                            <label class="col-md-2" >Date:</label>
                            <input id="datetimepicker-1" type="text" name="date">
                        </div>

                        <div class="form-group">
                            <label class="col-md-2" >Name:</label>
                            <select class="col-md-3" name="eid" id="eid">
                                <option value="" hidden></option>
                                <?php
                                $positionSelect = '';
                                if (!($stmt = $mysqli->prepare('SELECT E.fname, E.lname, R.id, E.id, R.city
                                                                FROM mby_employee E
                                                                INNER JOIN mby_restaurant R ON R.id = E.restaurant_id')));
                                {
                                    echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                }

                                if (!$stmt->execute()) {
                                    echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                if (!$stmt->bind_result($fname, $lname, $rid, $eid, $city)) {
                                    echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                while ($stmt->fetch()) {
                                    echo "<option name=\"eid\" value=\"" . $eid . "\" data-rid=\"". $rid ."\" data-city=\"". $city ."\" data-fname=\"". $fname ."\" data-lname=\"". $lname ."\">" . $fname . " " . $lname . "</option>";
                                }
                                $stmt->close();

                                ?>
                            </select>
                            <input type="hidden" id="fname" name="fname" value="">
                            <input type="hidden" id="lname" name="lname" value="">
                        </div>

                        <div class="form-group">
                            <label class="col-md-2" >Restaurant Location:</label>
                            <input class="col-md-3" type="text" id="city" name="city" readonly="readonly" value="">
                            <input type="hidden" id="rid" name="rid" value="">
                        </div>


                        <div class="form-group">
                            <label class="col-md-2" >Menu Item:</label>
                            <select class="col-md-3" name="mName" id="mName">
                                <option value="" hidden></option>
                                <?php
                                if (!($stmt = $mysqli->prepare('SELECT M.id, M.name FROM mby_menu_items M ORDER BY M.id ASC')));
                                {
                                    echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                }

                                if (!$stmt->execute()) {
                                    echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                if (!$stmt->bind_result($mid, $mName)) {
                                    echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                }

                                while ($stmt->fetch()) {
                                    echo "<option name=\"mid\" value=\"" . $mid ."\">" . $mName . "</option>";
                                }
                                $stmt->close();

                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2" >Quantity Sold:</label>
                            <input class="col-md-3" type="text" id="qty" name="qty" value="">
                        </div>

                        <input type="hidden" name="mode" value="add">
                        <input name="submit" value="Add Sale" class="btn btn-primary" type="submit">
                    </fieldset>
                </form>

            </div>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Date</th>
                        <th>Item Code</th>
                        <th>Item Name</th>
                        <th>Quantity Sold</th>
                        <th>Price</th>
                        <th>Location</th>
                        <th>Emp Name</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php
                    //contains basic logic for delete, update, and setting mode
                    include "includes/logic.inc.php";
                    unset($params);
                    unset($data);

                    //used in insert and update functions. mode distinguishes which is used.
                    if (isset($_POST['date']) && !empty($_POST['date'])) {
                        $postDate = $_POST['date'];
                    }
                    if (isset($_POST['fname']) && !empty($_POST['fname'])) {
                        $postFirstName = $_POST['fname'];
                    }
                    if (isset($_POST['lname']) && !empty($_POST['lname'])) {
                        $postLastName = $_POST['lname'];
                    }
                    if (isset($_POST['city']) && !empty($_POST['city'])) {
                        $postCity = $_POST['city'];
                    }
                    if (isset($_POST['rid']) && !empty($_POST['rid'])) {
                        $postRID = $_POST['rid'];
                    }
                    if (isset($_POST['eid']) && !empty($_POST['eid'])) {
                        $postEID = $_POST['eid'];
                    }
                    if (isset($_POST['mName']) && !empty($_POST['mName'])) {
                        $postMID = $_POST['mName'];
                    }
                    if (isset($_POST['qty']) && !empty($_POST['qty'])) {
                        $postQty = $_POST['qty'];
                    }


                    $getSql = 'SELECT SA.date, M.id, M.name, SA.item_quantity, M.price, R.city, E.fname, E.lname, SA.id
                                  FROM mby_sales SA
                                  INNER JOIN mby_menu_items M ON SA.item_id = M.id
                                  INNER JOIN mby_restaurant R ON SA.restaurant_id = R.id
                                  LEFT JOIN mby_employee E ON SA.employee_id = E.id
                                  ORDER BY SA.date DESC, R.city ASC, E.lname ASC';
                    $getParams = array(['date', 'menu_id','menu_name', 'sa_qty','menu_price','r_city', 'fname', 'lname', 'id']);

                    if ($modeSet) {
                        if ($mode === "add") {
                            $sql = 'INSERT INTO mby_sales(restaurant_id, employee_id, item_id, item_quantity, date) VALUES (?,?,?,?,?)';
                            $data = ['rid'=>$postRID, 'eid'=>$postEID, 'mid'=>$postMID, 'qty'=>$postQty, 'date'=>$postDate];
                            $params = 'iiiis';
                            $action = new mbySale($data, $params, $sql, $mysqli);
                            insertSale($action);

                            $action = new mbySale('', $getParams, $getSql, $mysqli);
                            getSale($action);
                            unset($action);
                        }
                        elseif ($mode === "delete") {
                            $sql = 'DELETE FROM mby_sales WHERE id = ?';
                            $params = 'i';
                            $action = new mbySale($getID, $params, $sql, $mysqli);
                            deleteSale($action);

                            $action = new mbySale('', $getParams, $getSql, $mysqli);
                            getSale($action);
                            unset($action);
                        }
                        elseif ($mode === "edit") {
                            $action = new mbySale('', $getParams, $getSql, $mysqli);
                            getSale($action);
                            unset($action);
                        }
                        elseif ($mode === "update") {
                            unset($data);
                            $data = ['rid'=>$postRID, 'eid'=>$postEID, 'mid'=>$postMID, 'qty'=>$postQty, 'date'=>$postDate, 'id'=>$postID];
                            $sql = 'UPDATE mby_sales SET restaurant_id = ?, employee_id = ?, item_id = ?, item_quantity = ?, date = ? WHERE id = ?';
                            $params = 'iiiisi';
                            $action = new mbySale($data, $params, $sql, $mysqli);
                            updateSale($action);

                            $action = new mbySale('', $getParams, $getSql, $mysqli);
                            getSale($action);
                            unset($action);
                        }
                    }
                    else {

                        $action = new mbySale('', $getParams, $getSql, $mysqli);
                        getSale($action);
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function getSale($action)
                    {
                        $rData = $action->show();
                        unset($action);
                        echo $rData;
                    }

                    /**
                     * @param $action
                     */
                    function insertSale($action)
                    {
                        $rData = $action->insert();
                        echo $rData;
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function deleteSale($action) {
                        $rData = $action->remove();
                        echo $rData;
                        unset($action);
                    }

                    /**
                     * @param $action
                     */
                    function updateSale($action) {
                        $rData = $action->update();
                        echo $rData;
                        unset($action);
                    }


                    ?>
                    </tbody>
                </table>
                </form>
            </div>

            <!-- Begin Modal -->
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Update Sale</h4>
                        </div>
                        <div class="modal-body">
                            <form id="updateForm" class="form-horizontal" method="POST" action="sales.php">
                                <input type="hidden" name="mode" value="update">
                                <?php
                                if (isset($_GET["id"]) && !empty($_GET["id"]) && isset($_GET["mode"]) && !empty($_GET["mode"])) {
                                    $getID = $_GET['id'];
                                    $mode = $_GET['mode'];
                                }
                                if (isset($mode) && !empty($mode)) {
                                    if ($mode === "edit") {
                                        if (!($stmt = $mysqli->prepare("SELECT SA.date, M.id, M.name, SA.item_quantity, M.price, R.city, R.id, E.id, E.fname, E.lname, SA.id
                                                                        FROM mby_sales SA
                                                                        INNER JOIN mby_menu_items M ON SA.item_id = M.id
                                                                        INNER JOIN mby_restaurant R ON SA.restaurant_id = R.id
                                                                        LEFT JOIN mby_employee E ON SA.employee_id = E.id
                                                                        WHERE SA.id = ?
                                                                        "))
                                        ) {
                                            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!($stmt->bind_param('i', $getID))) {
                                            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
                                        }
                                        if (!$stmt->execute()) {
                                            echo "Execute failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }
                                        if (!$stmt->bind_result($date, $mid, $item, $qty, $price, $city, $rid, $eid, $fname, $lname, $id)) {
                                            echo "Bind failed: " . $mysqli->connect_errno . " " . $mysqli->connect_error;
                                        }

                                        $stmt->fetch();
                                        $stmt->close();

                                        echo "<div class=\"form-group\">
                                                <label class=\"col-md-3\" >Name:</label>
                                                    <select class=\"col-md-5\" name=\"eid\" id=\"employees\">
                                                        <option value=\"\" hidden></option>";

                                                        if (!($stmt = $mysqli->prepare('SELECT R.id, E.id, E.fname, E.lname
                                                                                        FROM mby_employee E
                                                                                        INNER JOIN mby_restaurant R ON R.id = E.restaurant_id')));
                                                        {
                                                            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                                        }

                                                        if (!$stmt->execute()) {
                                                            echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                                        }

                                                        if (!$stmt->bind_result($restaurant_id, $employee_id, $first, $last)) {
                                                            echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                                        }

                                                        while ($stmt->fetch()) {
                                                            if ($eid !== $employee_id) {
                                                                echo "<option name=\"eid\" value=\"" . $employee_id . "\" data-restid=\"". $restaurant_id ."\" data-first=\"". $first ."\" data-last=\"". $last ."\">" . $first . " " . $last . "</option>";

                                                            }
                                                            else {
                                                                echo "<option name=\"eid\" value=\"" . $employee_id . "\" data-restid=\"". $restaurant_id ."\" data-fname=\"". $first ."\" data-lname=\"". $last ."\" selected>" . $first . " " . $last . "</option>";
                                                            }
                                                        }
                                                        $stmt->close();




                                        echo "</select>
                                            </div>
                                            <input type=\"hidden\" name=\"id\" value=\"". $id ."\">
                                            <input type=\"hidden\" id=\"r_id\" name=\"rid\" value=\"" . $restaurant_id . "\">


                                            <div class=\"form-group\">
                                                <label class=\"col-md-3\" >Date:</label>
                                                <input id=\"datetimepicker-4\" type=\"text\" name=\"date\" value=\"". $date . "\">
                                            </div>

                                            <div class=\"form-group\">
                                                <label class=\"col-md-3\" >Menu Item:</label>
                                                <select class=\"col-md-5\" name=\"mName\" id=\"mName\">
                                                    <option value=\"\" hidden></option>";
                                                    if (!($stmt = $mysqli->prepare('SELECT M.id, M.name FROM mby_menu_items M ORDER BY M.id ASC')));
                                                    {
                                                        echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
                                                    }

                                                    if (!$stmt->execute()) {
                                                        echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                                    }

                                                    if (!$stmt->bind_result($item_id, $item_name)) {
                                                        echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
                                                    }

                                                    while ($stmt->fetch()) {
                                                        if ($item_id !== $mid) {
                                                            echo "<option name=\"mid\" value=\"" . $item_id . "\">" . $item_name . "</option>";
                                                        }
                                                        else {
                                                            echo "<option name=\"mid\" value=\"" . $item_id . "\" selected>" . $item_name . "</option>";
                                                        }
                                                    }
                                                    $stmt->close();
                                        echo "</select>
                                            </div>
                                            <div class=\"form-group\">
                                                <label class=\"col-md-3\" >Quantity:</label>
                                                <input type=\"text\" name=\"qty\" value=\"". $qty . "\">
                                            </div>";

                                    }

                                }

                                ?>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <input name="submitEdit" value="Save" class="btn btn-primary" type="submit">
                        </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
        </div>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>

    <script>
        $(document).ready(function(){
            var queryString = window.location.search;
            queryString = queryString.substring(1);
            var params = parseQueryString(queryString);

            if (params["mode"] === "edit")
            {
                var filename = getFileName();

                console.log(params);
                console.log(filename);
                $('#myModal').modal('show');
            }

        });

        $('#eid').change(function() {
            $( "select#eid option:selected" ).each(function() {
                $( "#position" ).val($( this ).attr('data-position'));
                $( "#pid" ).val($( this ).attr('data-pid'));
                $( "#city" ).val($( this ).attr('data-city'));
                $( "#rid" ).val($( this ).attr('data-rid'));
                $( "#fname" ).val($( this ).attr('data-fname'));
                $( "#lname" ).val($( this ).attr('data-lname'));
            });

        });

        $('#employees').change(function() {
            $( "select#employees option:selected" ).each(function() {
                $( "#position" ).val($( this ).attr('data-title'));
                $( "#pid" ).val($( this ).attr('data-titleid'));
                $( "#r_id" ).val($( this ).attr('data-restid'));
                $( "#fname" ).val($( this ).attr('data-first'));
                $( "#lname" ).val($( this ).attr('data-last'));
            });

        });


        $("#position").css("background-image", "");


        $('#datetimepicker-1').datetimepicker({
            timepicker:false,
            format: 'Y-m-d',
            mask:true, // '2016/12/31
            minDate:'2016/03/01',//yesterday is minimum date(for today use 0 or -1970/01/01)
            maxDate:'2018/01/02',
            startDate: '2016/03/01'
        });

        $('#datetimepicker-4').datetimepicker({
            timepicker:false,
            format: 'Y-m-d',
            mask:true, // '2016/12/31
            minDate:'2016/03/01',//yesterday is minimum date(for today use 0 or -1970/01/01)
            maxDate:'2018/01/02',
            startDate: '2016/03/01'
        });

        function parseQueryString( queryString ) {
            var params = {}, queries, temp, i, l;

            // Split into key/value pairs
            queries = queryString.split("&");

            // Convert the array of strings into an object
            for ( i = 0, l = queries.length; i < l; i++ ) {
                temp = queries[i].split('=');
                params[temp[0]] = temp[1];
            }

            return params;
        };

        function getFileName() {
            //this gets the full url
            var url = document.location.href;
            //this removes the anchor at the end, if there is one
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
            //this removes the query after the file name, if there is one
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
            //this removes everything before the last slash in the path
            url = url.substring(url.lastIndexOf("/") + 1, url.length);
            return url;
        }
    </script>

    </body>
    </html>
<?php
//close database connection
$mysqli->close();
?>