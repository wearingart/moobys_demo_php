<?php

namespace Moobys;
require 'ActionInterface.php';


class Restaurant implements ActionInterface
{
    protected $data;
    protected $sql;
    protected $params;
    protected $mysqli;


    public function __construct($data,$params,$sql,$mysqli)
    {
        $this->data = $data;
        $this->params = $params;
        $this->sql = $sql;
        $this->mysqli = $mysqli;
    }

    public function show()
    {
        $restaurantList = '';

        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        if (!$stmt->bind_result($this->params['id'], $this->params['address'], $this->params['city'], $this->params['state'], $this->params['zip'])) {
            echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        while ($stmt->fetch()) {
            $restaurantList = $restaurantList . "<tr>\n<td style=\"width:20px;\"><a href=\"restaurants.php?id=".$this->params['id']."&mode=edit\"  style=\"color:brown\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a></button></td>\n<td style=\"width:20px;\"><a href=\"restaurants.php?id=".$this->params['id']."&mode=delete\" style=\"color:red\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button></td>\n<td>\n" . $this->params['id'] . "\n</td>\n<td>\n" . $this->params['address'] . "\n</td>\n<td>\n" . $this->params['city'] . "\n</td>\n<td>\n" . $this->params['state'] . "\n</td>\n<td>\n" . $this->params['zip'] . "\n</td></tr>";
        }
        $stmt->close();

        return $restaurantList;
    }

    public function update()
    {
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data['address'], $this->data['city'], $this->data['state'], $this->data['zip'], $this->data['id']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                Updated " . $stmt->affected_rows . " row in mby_restaurant.
                </div></p>";
        }
        $stmt->close();
    }


    public function insert()
    {
        $insertRest = '';

        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data['address'], $this->data['city'], $this->data['state'], $this->data['zip']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        } else {
            $insertRest = "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        Added " . $stmt->affected_rows . " rows to mby_restaurant.
                        </div></p>";
        }
        $stmt->close();

        return $insertRest;
    }

    public function remove()
    {
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                    Removed " . $stmt->affected_rows . " row from mby_restaurant.
                    </div></p>";
        }
        $stmt->close();
    }


}