<?php
/**
 * Created by PhpStorm.
 * User: brian
 * Date: 3/12/16
 * Time: 7:02 PM
 */

namespace Moobys;
require "ActionInterface.php";

class Schedule implements ActionInterface
{
    protected $data;
    protected $sql;
    protected $params;
    protected $mysqli;


    public function __construct($data,$params,$sql,$mysqli)
    {
        $this->data = $data;
        $this->params = $params;
        $this->sql = $sql;
        $this->mysqli = $mysqli;
    }

    public function show()
    {
        $employeeList = '';

        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        if (!$stmt->bind_result($this->params['date'], $this->params['city'], $this->params['eid'], $this->params['fname'], $this->params['lname'], $this->params['position'], $this->params['starttime'], $this->params['endtime'], $this->params['rid'], $this->params['pid'], $this->params['id'])) {
            echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }
        while ($stmt->fetch()) {
            $employeeList = $employeeList . "<tr>\n<td style=\"width:20px;\"><a href=\"schedules.php?id=".$this->params['id']."&mode=edit\"  style=\"color:brown\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a></button></td>\n<td style=\"width:20px;\"><a href=\"schedules.php?id=".$this->params['id']."&mode=delete\" style=\"color:red\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button></td>\n
            <td>" . $this->params['date'] . "</td>\n<td>" . $this->params['city'] . "</td>\n<td>" . $this->params['eid'] . "</td>\n<td>\n" . $this->params['fname'] . "</td>\n<td>" . $this->params['lname'] . "</td>\n<td>" . $this->params['position'] . "</td>\n<td>" . $this->params['starttime'] . "</td>\n<td>" . $this->params['endtime'] . "\n</td></tr>";
        }
        $stmt->close();

        return $employeeList;
    }

    public function update()
    {
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data['restaurant'], $this->data['position'], $this->data['fname'], $this->data['lname'], $this->data['hourly'], $this->data['phone'], $this->data['id']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                Updated " . $stmt->affected_rows . " row in mby_restaurant.
                </div></p>";
        }
        $stmt->close();
    }

    public function insert()
    {
        $insertRest = '';

        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data['eid'], $this->data['date'], $this->data['starttime'], $this->data['endtime']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        } else {
            $insertRest = "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        Added " . $stmt->affected_rows . " rows to mby_restaurant.
                        </div></p>";
        }
        $stmt->close();

        return $insertRest;
    }

    public function remove()
    {
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                    Removed " . $stmt->affected_rows . " row from mby_restaurant.
                    </div></p>";
        }
        $stmt->close();
    }
}