<?php
/**
 * Created by PhpStorm.
 * User: brian
 * Date: 3/13/16
 * Time: 3:07 AM
 */

namespace Moobys;
require "ActionInterface.php";

class Sale implements ActionInterface
{
    protected $data;
    protected $sql;
    protected $params;
    protected $mysqli;


    public function __construct($data,$params,$sql,$mysqli)
    {
        $this->data = $data;
        $this->params = $params;
        $this->sql = $sql;
        $this->mysqli = $mysqli;
    }

    public function show()
    {
        $salesList = '';

        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        if (!$stmt->bind_result($this->params['date'], $this->params['menu_id'], $this->params['menu_name'], $this->params['sa_qty'], $this->params['menu_price'], $this->params['r_city'], $this->params['fname'], $this->params['lname'], $this->params['id'])) {
            echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        while ($stmt->fetch()) {
            $salesList = $salesList . "<tr>\n<td style=\"width:20px;\"><a href=\"sales.php?id=".$this->params['id']."&mode=edit\"  style=\"color:brown\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a></button></td>\n<td style=\"width:20px;\"><a href=\"sales.php?id=".$this->params['id']."&mode=delete\" style=\"color:red\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button>
            </td>\n<td>" . $this->params['date'] . "</td>\n<td>" . $this->params['menu_id'] . "</td><td>" . $this->params['menu_name'] . "</td>\n<td>" . $this->params['sa_qty'] . "</td>\n<td>\$" . $this->params['menu_price'] . "</td>\n<td>" . $this->params['r_city'] . "</td>\n<td>" . $this->params['fname'] . "</td>\n<td>\n" . $this->params['lname'] . "</td>\n</tr>";
        }
        $stmt->close();

        return $salesList;
    }

    public function update()
    {
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data['rid'], $this->data['eid'], $this->data['mid'], $this->data['qty'], $this->data['date'], $this->data['id']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                Updated " . $stmt->affected_rows . " row in mby_restaurant.
                </div></p>";
        }
        $stmt->close();
    }

    public function insert()
    {
        $insertMenuItem = '';

        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data['rid'], $this->data['eid'], $this->data['mid'], $this->data['qty'], $this->data['date']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        } else {
            $insertMenuItem = "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        Added " . $stmt->affected_rows . " rows to mby_restaurant.
                        </div></p>";
        }
        $stmt->close();

        return $insertMenuItem;
    }

    public function remove()
    {
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                    Removed " . $stmt->affected_rows . " row from mby_restaurant.
                    </div></p>";
        }
        $stmt->close();
    }
}