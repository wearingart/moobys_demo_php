<?php
/*
 * Menu Class
 *
 * This class provides functions used to show, update, insert, and delete data from the Moobys database.
 *
 */


namespace Moobys;
require 'ActionInterface.php';

class Menu implements ActionInterface
{
    protected $data;
    protected $sql;
    protected $params;
    protected $mysqli;


    /**
     * Menu constructor.
     * @param $data
     * @param $params
     * @param $sql
     * @param $mysqli
     */
    public function __construct($data, $params, $sql, $mysqli)
    {
        $this->data = $data;
        $this->params = $params;
        $this->sql = $sql;
        $this->mysqli = $mysqli;
    }

    /**
     * @desc shows all data from the menu table
     * @return string
     */
    public function show()
    {
        $employeeList = '';

        //prepares SQL statement to be run
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        //executes SQL statement
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        //binds results to variables
        if (!$stmt->bind_result($this->params['id'], $this->params['name'], $this->params['price'], $this->params['description'])) {
            echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        //fetches data a row at a time and generates code to display results
        while ($stmt->fetch()) {
            $employeeList = $employeeList . "<tr>\n<td style=\"width:20px;\"><a href=\"menu.php?id=".$this->params['id']."&mode=edit\"  style=\"color:brown\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a></button></td>\n<td style=\"width:20px;\"><a href=\"menu.php?id=".$this->params['id']."&mode=delete\" style=\"color:red\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button></td>\n<td>" . $this->params['id'] . "</td>\n<td>" . $this->params['name'] . "</td>\n<td>\$" . $this->params['price'] . "</td>\n<td>\n" . $this->params['description'] . "</td>\n</tr>";
        }
        $stmt->close();

        return $employeeList;
    }

    /**
     * @desc updates a row in the menu table
     */
    public function update()
    {
        //prepares SQL statement to be run
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        //binds parameters to variables that will be passed to execute
        if (!($stmt->bind_param($this->params, $this->data['name'], $this->data['price'], $this->data['description'], $this->data['id']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }

        //executes SQL statement
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            //Generates a message to indicate success.
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                Updated " . $stmt->affected_rows . " row in mby_restaurant.
                </div></p>";
        }
        $stmt->close();
    }

    /**
     * @desc inserts a row of data into the menu table
     * @return string
     */
    public function insert()
    {
        $insertMenuItem = '';

        //prepares SQL statement to be run
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        //binds parameters to variables that will be passed to execute
        if (!($stmt->bind_param($this->params, $this->data['name'], $this->data['price'], $this->data['description']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }

        //executes SQL statement
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        } else {
            //Generates alert to indicate success
            $insertMenuItem = "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        Added " . $stmt->affected_rows . " rows to mby_restaurant.
                        </div></p>";
        }
        $stmt->close();

        return $insertMenuItem;
    }

    /**
     * @desc removes a record from the menu table
     */
    public function remove()
    {

        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        if (!($stmt->bind_param($this->params, $this->data))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                    Removed " . $stmt->affected_rows . " row from mby_restaurant.
                    </div></p>";
        }
        $stmt->close();
    }
}