<?php
/*
 * Positions Class
 *
 * This class provides functions used to show, update, insert, and delete data from the Moobys database.
 *
 */


namespace Moobys;
require 'ActionInterface.php';


class Positions implements ActionInterface
{
    protected $data;
    protected $sql;
    protected $params;
    protected $mysqli;


    /**
     * Positions constructor.
     * @param $data
     * @param $params
     * @param $sql
     * @param $mysqli
     */
    public function __construct($data, $params, $sql, $mysqli)
    {
        $this->data = $data;
        $this->params = $params;
        $this->sql = $sql;
        $this->mysqli = $mysqli;
    }

    /**
     * Shows all data from the positions table
     * @return string
     */
    public function show()
    {
        $positionsList = '';

        //prepares SQL statement to be run
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        //executes SQL statement
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        //binds results to variables
        if (!$stmt->bind_result($this->params['id'], $this->params['title'])) {
            echo "Bind failed: " . $stmt->connect_errno . " " . $stmt->connect_error;
        }

        //fetches data a row at a time and generates code to display results
        while ($stmt->fetch()) {
            $positionsList = $positionsList . "<tr>\n<td style=\"width:20px;\"><a href=\"positions.php?id=".$this->params['id']."&mode=edit\"  style=\"color:brown\"><span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span></a></button></td>\n<td style=\"width:20px;\"><a href=\"positions.php?id=".$this->params['id']."&mode=delete\" style=\"color:red\"><span class=\"glyphicon glyphicon-remove\" aria-hidden=\"true\"></span></button></td>\n<td>\n" . $this->params['id'] . "\n</td>\n<td>\n" . $this->params['title'] . "\n</td></tr>";
        }
        $stmt->close();

        return $positionsList;
    }

    /**
     *Updates a record in the positions database
     */
    public function update()
    {

        //prepares SQL statement to be run
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        //binds parameters to variables that will be passed to execute
        if (!($stmt->bind_param($this->params, $this->data['title'], $this->data['id']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }

        //executes SQL statement
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            //Generates a message to indicate success.
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                Updated " . $stmt->affected_rows . " row in mby_restaurant.
                </div></p>";
        }
        $stmt->close();
    }


    /**
     * @desc Inserts a new record into the positions database
     * @return string
     */public function insert()
    {
        $insertRest = '';

        //prepares SQL statement to be run
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        //binds parameters to variables that will be passed to execute
        if (!($stmt->bind_param($this->params, $this->data['title']))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }

        //executes SQL statement
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        } else {
            //Generates alert to indicate success
            $insertRest = "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                        Added " . $stmt->affected_rows . " rows to mby_restaurant.
                        </div></p>";
        }
        $stmt->close();

        return $insertRest;
    }

    /**
     * @desc removes a row from the positions table
     */
    public function remove()
    {
        //prepares SQL statement to be run
        if (!($stmt = $this->mysqli->prepare($this->sql))) {
            echo "Prepare failed: " . $stmt->errno . " " . $stmt->error;
        }

        //binds parameters to variables that will be passed to execute
        if (!($stmt->bind_param($this->params, $this->data))) {
            echo "Bind failed: " . $stmt->errno . " " . $stmt->error;
        }

        //executes SQL statement
        if (!$stmt->execute()) {
            echo "Execute failed: " . $stmt->errno . " " . $stmt->error;
        }
        else {
            //Generates alert to indicate success
            echo "<p><div class=\"alert alert-info alert-dismissible\" role=\"alert\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                    Removed " . $stmt->affected_rows . " row from mby_restaurant.
                    </div></p>";
        }
        $stmt->close();
    }


}