<?php
/*
 * ActionInterface interface file used by classes in Moobys namespace
 *
 */

namespace Moobys;


interface ActionInterface
{
    public function show();

    public function update();

    public function insert();

    public function remove();

}